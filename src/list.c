/*
 *	File: list.c
 *
 *	Functions for a general all-purpose linked list implementation.
 *
 *	Author: Preben Vangberg <prv@aber.ac.uk>
 *	Date 12.11.2018
 *	
 * 	List.c is a general all-purpose sorted linked list that is built
 * 	up by nodes that points to other nodes, and that contains a pointer
 * 	to any data. As the list is entirely built up by nodes with
 * 	pointers it does not interfere with other list containing
 * 	the same data.
 *
 * 	A list is defined by a pointer to the first node of a list.
 *
 * 	Copyright (C) 2018, Preben Vangberg
 *	Licensed under the MIT license.
 */

#include <stdlib.h>
#include <stdio.h>
#include "list.h"

#include "module.h"


static node* create_node(void *data);


/*
 *	Adds the item to the the given list in
 *	sorted order. This is based on the compare
 *	function given.
 */
void add_to_list(node **head, void *data, int(*compare)(const void *e1, const void *e2))
{
    node *element = create_node(data);
	if((*head) == NULL)
		(*head) = element;
	else if(compare(element->data, (*head)->data) < 0){
		element->next = (*head);
		(*head) = element;
	} else {
		node *pointer = (*head);
		while(pointer->next && compare(element->data, pointer->next->data) > 0)
			pointer = pointer->next;
		element->next = pointer->next;
		pointer->next = element;
	}
}


/*
 *	Adds all the elements found in
 *	a given file to the list.
 *
 *	Returns number of element added
 */
int add_file(node **head, char *file, void* (*read_element)(FILE *f), int (*compare)(const void *e1, const void *e2))
{
	FILE *f = fopen(file, "r");
	if(f == NULL)
		return 0;
	int number_of_elements = 0;
	void *element;
	while((element = read_element(f))) {
		add_to_list(head, element, compare);
		number_of_elements++;
	}
	return number_of_elements;
}


/*
 *	Execute a function on all the elements of a list.
 *
 * 	Returns NULL if successfull or last element if it failed.
 * 	The function that is called need to return 0 if it is
 * 	successful, or it is considered a failure.
 */
void* execute_for_each(node *head, int (*function)(void *element))
{
	while(head){
		if(function(head->data) == 0)
			head = head->next;
		else
			return head;
	}
	return head;
}


/*	
 *	Removes the element from the list
 *	then frees the memory used by the
 *	element and the data.
 *
 *	NOTE: Only use this if you are 100%
 *	certain that using free on the node
 *	will result in it being 100%
 *	de-allocated.
 */
void delete_node(node **head, node *node)
{	
	free(node->data);
	remove_node(head, node);
}


/*
 * Clears the list, de-allocates the memory
 * for all the nodes, and sets the head to
 * NULL.
 */
void clear_list(node **head)
{
	node *p = (*head);
	while(p){
		node *next = (*p).next;
		free(p);
		p = next;		
	}	
	*head = NULL;
}


/*
 *	Removes the element from the list
 *	and free the memory used by the
 *	node.
 *
 *	Returns the data from the node that
 *	was deleted, or NULL if no one was
 *	deleted
 */
void* remove_node(node **head, node *n)
{
	if(*head == n){
		node *new = (*head);
		(*head) = n->next;
		return new;
	} 
	node *p = *head;
	while(p->next && p->next != n)
		p = p->next;
	if(n == p->next){
		void *data =((node*)p->next)->data;
		p->next = p->next->next;
		free(n);
		return data;
	}
	return NULL;
}


/*
 *	Searches for the first element that returns
 *	0 from the compare function
 *
 *	Returns a pointer to the data if it is found.
 *	Else it returns NULL.
 */
void* get_element(node *head, void *element, int(*compare)(const void *e1, const void*e2))
{	
	while(head){
		int result = compare(head->data, element);
		if(result == 0)
			return head->data;
		if(result > 0)
			return NULL;
		head = head->next;
	}
	return NULL;
}


/*
 *	Takes a pointer to the data then
 *	creates a node with the data.
 *
 *	Returns a pointer to the new node.
 */
static node* create_node(void *data)
{
	node *n = malloc(sizeof(node));
	n->next = NULL;
	n->data = data;
	return n;
}


