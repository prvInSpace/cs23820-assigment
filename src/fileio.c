/*
 *	File: fileio.c
 *
 *	Contains general functions for file interaction.
 *
 *	Author: Preben Vangberg <prv@aber.ac.uk>
 *	Date 12.11.2018
 *	
 *	fileio.c contains functions that is used to prompt the
 *	user for folders, check if files and and folders exist,
 *	and other general file interactions.
 *
 * 	Copyright (C) 2018, Preben Vangberg
 *	Licensed under the MIT license.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "fileio.h"
#include "list.h"


static int check_for_files(char *folder);
static int exist(char *file);


/*
 * List of the files that needs to be inside the
 * directory for the main problem to work.
 */
static char *files[] = {
	"modules.txt", 
	"schemes.txt",
	"times.txt"
};


/*
 *	Prompts the user for a folderpath, and checks whether
 *	or not it as a folder, file, or invalid. Also checks
 *	if the folder contains the neccesary files to complete
 *	the main problem.
 *
 *	Returns a char* that contains the path of the folder.
 */
char* get_folder()
{
	char buffer[100];
	while(1){
		printf("Please enter the folder to load: ");
		read_string(buffer, sizeof(buffer));
		struct stat info;
		if(stat(buffer, &info) != 0)
			printf("ERROR: Could not locate %s!\n", buffer);
		else if(info.st_mode & S_IFDIR) {
			if(check_for_files(buffer))
				break;
		} else {
			printf("ERROR: %s is not a directory!\n", buffer);
		}	
	}

	int length = strlen(buffer) + 1;
	char *folder = malloc(length * sizeof(char));
	strcpy(folder, buffer);
	return folder;	
}


/*
 *	Read a string and remove the tailing \r\n
 *
 * 	This function uses fgets which is a safer way of reading a
 * 	string as it takes a size as its parameter. It have one
 * 	drawback in that it leaves a tailing \r\n on the end. This
 * 	function removes that.
 *
 *  Returns the length of the string
 */
int read_string(char *buffer, int size)
{
	fgets(buffer, size, stdin);
	int index = strcspn(buffer, "\r\n");
	buffer[index] = '\0';
	return index;
}

/*
 *	Concatinates the folder and file together with a seperator,
 *	and calls the list.c function that adds all elements in a file
 *	to a certain list.
 *
 *	Returns the number of elements added.
 */
int import_file(char *folder, char *file, void *head, void* (*read_file)(FILE *f), int(*compare)(const void *e1, const void *e2))
{
	//1 extra for '\0' and 1 extra for '/'
	char path[strlen(folder) + strlen(file) + 2];
	strcpy(path, folder);
	strcat(path, "/");
	strcat(path, file);
	printf("Importing file: %s\n", path);
	int nmodule = add_file(head, path, read_file, compare);
	if(nmodule == 0){
		printf("ERROR: Did not manage to import file!");
		exit(1);
	}
	return nmodule;
}


/*
 * Checks if the folder spesified actually have all
 * neccesary files.
 *
 * returns 0 if some files are missing.
 * return 1 if all files exist.
 */
static int check_for_files(char *folder)
{	
	int return_value = 1;
	char buffer[120];
	int length = sizeof(files) / sizeof(files[0]);
	for(int i = 0; i < length; ++i){
		strcpy(buffer, folder);
		strcat(buffer, "/");
		strcat(buffer, files[i]);
		if(!exist(buffer)){
			return_value = 0;
			printf("ERROR: Missing file %s\n", files[i]);
		}
	}
	return return_value;
}


/*
 *	Checks if a file exist.
 *
 * 	Returns 0 if the file does not exist.
 *	Returns 1 if the file exist
 */
static int exist(char *file)
{
	struct stat f;
	return (stat(file, &f) == 0);

}
