/*
 *	File: room.c
 *
 *	Implementation and import of the rooms struct.
 *
 *	Author: Preben Vangberg <prv@aber.ac.uk>
 *	Date 12.11.2018
 *
 *	This file contains the rooms struct and the
 *	functions that imports the rooms.txt file.
 *
 * 	Copyright (C) 2018, Preben Vangberg
 *	Licensed under the MIT license.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "main.h"

#include "room.h"


/*
 * Imports rooms.txt and puts the data into a new
 * rooms struct.
 *
 * Returns a pointer to a new rooms.
 */
rooms* import_rooms(char *folder)
{
	strcat(folder, "/rooms.txt");
	printf("Importing file: %s!\n", folder);
	FILE *f = fopen(folder, "r");

	if(f == NULL) {
		printf("WARNING: Could not locate %s!\n", folder);
		return NULL;
	}

	rooms r;
	r.l_rooms = -1;
	r.p_rooms = -1;

	char strings[2][10];
	int ints[2];
	int status = fscanf(f, "%s %d %s %d", strings[0], &ints[0], strings[1], &ints[1]);

	if(status != 4){
		printf("WARNING: Could not import rooms\n");
		if(WERROR)
			exit(1);
		return NULL;
	}

	// This is sanity test to make sure that we do not
	// have multiple entries for the same room type, and
	// that we do not insert a unknown room type.
	for(int i = 0; i < sizeof(strings) / sizeof(strings[0]); ++i){
		if(strcmp("L", strings[i]) == 0) {
			if(r.l_rooms == -1)
				r.l_rooms = ints[i];
			else
				printf("WARNING: Double entry of %s in rooms.txt", strings[i]);
		}
		else if(strcmp("P", strings[i]) == 0) {
			if(r.p_rooms == -1)
				r.p_rooms = ints[i];
			else
				printf("WARNING: Double entry of %s in rooms.txt", strings[i]);
		}
		else
			printf("WARNING: Unknown room type %s\n", strings[i]);
	}

	if(r.l_rooms == -1) {
		print_warning("No amount of lecture rooms set!\n");
	}
	if(r.p_rooms == -1) {
		print_warning("No amount of practicals rooms set!\n");
	}

	rooms *rp = malloc(sizeof(rooms));
	*rp = r;
	return rp;
}
