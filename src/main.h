/*
 *	File: main.h
 *
 *	The main file of the program.
 *
 *	Author: Preben Vangberg <prv@aber.ac.uk>
 *	Date 12.11.2018
 *
 *	main.h is the main file, and entry point to
 *	the program. It handles everything from the
 *	reading of arguments, and in a more general
 *	perspective what the program is supposed to
 *	do.
 *
 * 	Copyright (C) 2018, Preben Vangberg
 *	Licensed under the MIT license.
 */

#ifndef MAIN_H
#define MAIN_H

extern int DEBUG;
extern int WERROR;


/*
 * Prints a warning, and if the --werror flag is set it exit the program
 */
void print_warning(char *msg);

#endif
