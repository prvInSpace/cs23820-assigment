/*
 *	File: list.h
 *
 *	Functions for a general all-purpose linked list implementation.
 *
 *	Author: Preben Vangberg <prv@aber.ac.uk>
 *	Date 12.11.2018
 *
 * 	List.h is a general all-purpose sorted linked list that is built
 * 	up by nodes that points to other nodes, and that contains a pointer
 * 	to any data. As the list is entirely built up by nodes with
 * 	pointers it does not interfere with other list containing
 * 	the same data.
 *
 * 	A list is defined by a pointer to the first node of a list.
 *
 * 	The list does not allow duplicates of data in it.
 *
 * 	Copyright (C) 2018, Preben Vangberg
 *	Licensed under the MIT license.
 */

#ifndef LIST_H
#define LIST_H

#include <stdio.h>


/*
 * Represents a node in a list.
 * It contains a pointer to the next
 * element as well as a pointer to some
 * data
 */
typedef struct node_struct node;
struct node_struct {
	node *next;
	void *data;	
};


/*
 *	Adds the item to the the given list in
 *	sorted order. This is based on the compare
 *	function given.
 *
 *	It will refuse to add the element if it already exist.
 *
 *	Returns 1 if it did, or 0 if it didnt.
 *
 */
void add_to_list(node **head, void *data, int(*compare)(const void *e1, const void *e2));


/*
 *	Adds all the elements found in
 *	a given file to the list.
 *
 *	Returns number of element added
 */
int add_file(node **head, char *file, void* (*read_element)(FILE *f), int(*compare)(const void *e1, const void *e2));


/*
 *	Execute a function on all the elements of a list.
 *
 * 	Returns NULL if successfull or last element if it failed.
 * 	The function that is called need to return 0 if it is
 * 	successful, or it is considered a failure.
 */
void* execute_for_each(node *head, int (*function)(void *element));


/*
 *	Removes the element from the list
 *	then frees the memory used by the
 *	element and the data.
 */
void delete_node(node **head, node *node);


/*
 * Clears the list, deallocates the memory
 * for all the nodes, and sets the head to
 * NULL.
 */
void clear_list(node **head);


/*
 *	Removes the element from the list
 *	and free the memory used by the
 *	node.
 *
 *	Returns the data from the node that
 *	was deleted, or NULL if no one was
 *	deleted
 */
void* remove_node(node **head, node *node);


/*
 *	Searches for the first element that returns
 *	0 from the compare function
 *
 *	Returns a pointer to the data if it is found.
 *	Else it returns NULL.
 */
void* get_element(node *head, void *element, int(*compare)(const void *e1, const void*e2));

#endif

