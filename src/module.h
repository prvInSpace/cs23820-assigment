/*
 *	File: module.h
 *
 *	Implementation of a module struct and function related to this.
 *
 *	Author: Preben Vangberg <prv@aber.ac.uk>
 *	Date 12.11.2018
 *
 * 	Module.h contains functions related to importing, printing, and
 * 	comparing of modules. Also contains a linked list of all modules,
 * 	and functions to remove unnecessary modules from this list.
 *
 *  NOTE: Before freeing the memory used by a module, make sure to clear
 *  the list of clashes, as this list will not be de allocated when free
 *  is called.
 *
 * 	Copyright (C) 2018, Preben Vangberg
 *	Licensed under the MIT license.
 */

#ifndef MODULE_H
#define MODULE_H

#include "list.h"


/*
 * Struct that represents a module
 * It contains all the values need
 * inclusive a linked list with
 * modules that clashes.
 *
 * NB: When memory is de-allocated
 * the list is not sufficiently
 * deleted. Before deletion the
 * list needs to be cleared.
 */
typedef struct module_tag {
	char name[8];
	int semester;
	int students;
	int lectures;
	int lectures_length;	
   	int practicals;
 	int practicals_length;
	node *clashes;
} module;


/*
 * Linkedlist containing all modules.
 */
extern node *modules;


/*
 *	Reads a module from the given file.
 *
 *	Returns a pointer to the new
 *	module or it returns NULL if
 *	it failed.
 */
void* read_module(FILE *f);


/*
*	Prints a module.
*
*	Return 0. This is because
*	execute_for_each consider
*	0 a success.
*/
int print_module(void *p);


/*
 *	Compares two modules. This is
 *	based on the name of the module.
 *
 *	Returns the difference between the
 *	modules name.
 */
int compare_module(const void *e1, const void *e2);


/*
 *	Deletes all the modules with no
 *	students.
 *
 *	Returns number of deleted modules
 */
int delete_modules_with_no_students();

#endif
