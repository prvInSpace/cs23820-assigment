
#ifndef TABLE_H
#define TABLE_H

#include "module.h"
#include "room.h"
#include "list.h"

#define nDays 7
#define nHours 9

typedef struct timetable_struct {
	node *table[nDays][nHours];
} timetable;

typedef struct teaching_struct {
	char type;
	module *module;
} teaching;

timetable* build_timetable(int semester);
timetable* build_extended_timetable(int semester, rooms *rs);
void print_table(timetable *table);
void print_inverted_table(timetable *table);

#endif
