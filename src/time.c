/*
 *	File: time.c
 *
 *	Contains the binary-map of lecture times.
 *
 *	Author: Preben Vangberg <prv@aber.ac.uk>
 *	Date 12.11.2018
 *	
 *	Contains functions and structs for storing
 *	the data from times.txt. This information
 *	is stored in a binary-map where 0 represents
 *	no lecture, and 1 represents possibilities
 *	for lectures.
 *
 * 	Copyright (C) 2018, Preben Vangberg
 *	Licensed under the MIT license.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"

#include "time.h"


/*
 * Binary map that represents the
 * available hours.
 */
int times[nDays][nHours];


/*
 * Array of char arrays containing
 * the actual name of the weekdays
 * starting from monday
 */
char *weekdays[] = {
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday",
	"Sunday"
};


/*
 * Reads in the times.txt and
 * stores it in times.
 */
void read_time_file(char *folder)
{
	int line = 1;
	int days_found[nDays] = {0};
	char file[100];
	strcpy(file, folder);
	strcat(file, "/times.txt");
	printf("Importing file: %s!\n", file);

	FILE *f = fopen(file, "r");
	if(f == NULL){
		printf("ERROR: Failed to read %s!\n", file);
		exit(1);
	}	
	
	char buffer[20];
	int hours;
	int status;
	while((status = fscanf(f, "%s %d", buffer, &hours)) != EOF) {
		if(status != 2){
			printf("Failed to read 2 elements from %s!\n", file);
			printf("This is probably because of a syntax error on line %d\n", line);
			exit(1);
		}
		int day = -1;
		for(int i = 0; day == -1 && i < sizeof(weekdays) / sizeof(weekdays[0]); ++i){
			if(strcmp(weekdays[i], buffer) == 0)
				day = i;
		}
		if(day == -1){
			printf("Unknown day at line %d in times.txt!\n", line);
			exit(1);
		}
		days_found[day] = 1;
		int hour;
		for(int i = 0; i < hours; ++i){
			status = fscanf(f, "%d", &hour);
			times[day][hour - firstHour] = 1;
		}
		line++;
	}	

	for(int i = 0; i < nDays; ++i){
		if(days_found[i] == 0){
			printf("WARNING: No entry found for %s in times.txt!\n", weekdays[i]);
			if(WERROR)
				exit(1);
		}
	}
}


/*
 * Prints the binarymap.
 */
void print_time_map()
{
	printf("    M T W T F S S\n");
	printf("------------------\n");
	for(int x = 0; x < nHours; ++x){
		printf("|%2d|", x + firstHour);
		for(int y = 0; y < nDays; ++y)
			printf("%d|", times[y][x]);
		printf("\n");
	}
}

