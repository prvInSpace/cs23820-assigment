/*
 *	File: table.c
 *
 *	Implementation of the timetable.
 *
 *	Author: Preben Vangberg <prv@aber.ac.uk>
 *	Date 12.11.2018
 *	
 *	This file contains the definition of the
 *	timetable struct, and functions related
 *	to printing and creation of the timetables.
 *
 * 	Copyright (C) 2018, Preben Vangberg
 *	Licensed under the MIT license.
 */

#include <stdlib.h>

#include "main.h"
#include "module.h"
#include "scheme.h"
#include "table.h"
#include "time.h"

static timetable* create_table();
static int clashes_with_module(module *m, node *list);
static int compare_teachings(const void *t1, const void *t2);


/*
 *	Solution to the main problem.
 *
 *	Tries to create a timetable, only taking
 *	into consideration the modules and their
 *	clashes. It does not differansiate
 *	between lectures and practicals.
 *
 *	Returns a pointer to a new timetable.
 */
timetable* build_timetable(int semester)
{
	int missing = 0;

	//Initalization of timet
	timetable *timet = create_table();

	for(node *mp = modules; mp != NULL; mp = mp->next){
		module *m = (module*) mp->data; 
		if(m->semester != semester)
			continue;

		if(DEBUG){
			printf("Adding: ");
			print_module(m);
		}

		/*
		 * As we do not care whether or not we
		 * are dealing with either a lecture or
		 * practicals, we can just add them
		 * together.
		 */
		int total = m->lectures + m->practicals;

		/*
		 * This looks through the whole week, adding
		 * as many as it can. It only checks if there
		 * is an available slot at the given time, and
		 * that the module does not clash with any
		 * module currently occupying the space.
		 *
		 * As we know there can not be any available slot
		 * earlier, we only need to go trough the week
		 * once. This saves some runtime than if we were
		 * to check the whole week for each teaching
		 * session.
		 *
		 * It will increase the number of added teaching
		 * sessions, and if the number is not the same
		 * as the total number in the end, it will
		 * print a warning.
		 */
		int added = 0;
		for(int day = 0; day < nDays && added < total; ++day){
			for(int hour = 0; hour < nHours && added < total; ++hour){
				if(times[day][hour] == 0)
					continue;
				if(clashes_with_module(m, timet->table[day][hour]) == 0){
					teaching *t = malloc(sizeof(teaching));
					t->type = '\0';
					t->module = m;
					add_to_list(&timet->table[day][hour], t, &compare_teachings);
					added++;
				}					
			}
		}

		if(total != added) {
			if(missing == 0)
				printf("\n");
			printf("WARNING: No available space for %d sessions for %s!\n", total -added, m->name);
			if(WERROR)
				exit(1);
			missing += total - added;
		}
	}

	printf("\n");
	return timet;
}


/*
 *	Solution to the extended problem.
 *
 *	Creates a timetable, but takes into consideration
 *	the number of rooms, and length of the lectures
 *	and practicals.
 *
 *	Returns a pointer to the new timetable
 */
timetable* build_extended_timetable(int semester, rooms* rs)
{
	timetable *timet = create_table();

	//table to keep track of the number of rooms.
	rooms room_occapcy[nDays][nHours];
	for(int day = 0; day < nDays; ++day){
		for(int hour = 0; hour < nHours; ++hour){
			rooms r;
			r.l_rooms = rs->l_rooms;
			r.p_rooms = rs->p_rooms;
			room_occapcy[day][hour] = r;
		}
	}

	int error_found = 0;

	for(node *mp = modules; mp != NULL; mp = mp->next){
		module *m = (module*) mp->data;
		if(m->semester != semester)
			continue;

		if(DEBUG){
			printf("Adding: ");
			print_module(m);
		}

		int lecture = 0;
		int practicals = 0;

		/*
		 * This adds all the lectures to the timetable.
		 *
		 * It does this by going through all the available
		 * hours of the week, checking if it can place one.
		 * For it to place a lecture, there needs to be a slot
		 * at that time, there needs to be a room, and no module
		 * currently occupying the slot can clash with the
		 * module. If it passes all of these tests the length
		 * of the lecture forwards, it will then insert the
		 * lecture to the length of the lecture spots forward.
		 *
		 * As we know that there is not any available slot before
		 * the current one, we can simply skip the length of the
		 * teaching session forward and continue. This means
		 * we only go through the week once for each module.
		 * This is more efficient than checking the whole week
		 * per teaching session.
		 *
		 * It decreases the amount of lecture rooms and
		 * increases lecture if it manage to place one.
		 *
		 * If it reaches the end, and lecture is not the
		 * same as the amount of lectures, it prints a warning.
		 */
		for(int day = 0; day < nDays && lecture < m->lectures; ++day){
			for(int hour = 0; hour < nHours - m->lectures_length + 1 && lecture < m->lectures; ++hour){
				int hours_available;
				for(hours_available = 0; hours_available < m->lectures_length; ++hours_available){
					if(times[day][hour + hours_available] == 0
							|| room_occapcy[day][hour+hours_available].l_rooms <= 0
							|| clashes_with_module(m, timet->table[day][hour +  hours_available]) != 0)
						break;
				}

				if(hours_available != m->lectures_length){
					hour += hours_available;
					continue;
				}

				teaching *t = malloc(sizeof(teaching));
				t->type = 'L';
				t->module = m;
				lecture++;
				for(int i = 0; i < m->lectures_length; ++i){
					room_occapcy[day][hour + i].l_rooms--;
					add_to_list(&timet->table[day][hour + i], t, &compare_teachings);
				}
			}
		}	

		if(lecture != m->lectures){
			printf("WARNING: No available space for %d lectures for %s\n", m->lectures - lecture,  m->name);
			error_found = 1;
			if(WERROR)
				exit(1);
		}


		/*
		 * This does the same as above, only for practicals.
		 */
		for(int day = 0; day < nDays && practicals < m->practicals; ++day){
			for(int hour = 0; hour < nHours - m->practicals_length + 1 && practicals < m->practicals; ++hour){
				int hours_available;
				for(hours_available = 0; hours_available < m->practicals_length; ++hours_available){
					if(times[day][hour + hours_available] == 0
							|| room_occapcy[day][hour+hours_available].p_rooms <= 0
							|| clashes_with_module(m, timet->table[day][hour +  hours_available]) != 0)
						break;
				}

				if(hours_available != m->practicals_length){
					hour += hours_available;
					continue;
				}

				teaching *t = malloc(sizeof(teaching));
				t->type = 'P';
				t->module = m;
				practicals++;
				for(int i = 0; i < m->practicals_length; ++i){
					room_occapcy[day][hour + i].p_rooms--;
					add_to_list(&timet->table[day][hour + i], t, &compare_teachings);
				}
			}
		}	

		if(practicals != m->practicals){
			printf("WARNING: No available space for %d practicals for %s\n", m->practicals - practicals,  m->name);
			error_found = 1;
			if(WERROR)
				exit(1);
		}
	}


	if(error_found)
		printf("\n");

	return timet;
}


/*
 *	Prints the given timetable in a normal
 *	timetable form with the days on top and
 *	hours on the left hand side.
 */
void print_inverted_table(timetable *table)
{	
	printf(" Time   Monday   Tuesday  Wednesday  Thursday   Friday   Saturday   Sunday\n");
	printf("----------------------------------------------------------------------------\n");
	for(int hour = 0; hour < nHours; ++hour){
		int nextRound = -1;

		// This keeps track of the pointer for the
		// current day on the given hour.
		node *next[nDays];
		for(int i = 0; i < nDays; ++i)
			next[i] = table->table[i][hour];

		while(nextRound != 0){
			if(nextRound == -1)
				printf("| %2d |", hour + firstHour);
			else
				printf("|    |");
			nextRound = 0;
			for(int day = 0; day < nDays; ++day){
				if(next[day] == NULL)
					printf("%9s|", "");
				else {
					teaching *t = (teaching*)next[day]->data;
					printf(" %7s", t->module->name);
					if(t->type != '\0')
						printf("%c|", t->type);
					else
						printf(" |");
					next[day] = next[day]->next;
					if(next[day] != NULL)
						nextRound++;
				}
			}
			printf("\n");
		}
		printf("----------------------------------------------------------------------------\n");
	}
}


/*
 *	Prints a timetable in the default mode
 *	with days on the left hand side, and
 *	hours on the top.	
 */
void print_table(timetable *table)
{	
	printf("     Day         ");
	for(int i = 0; i < nHours; ++i)
		printf("%-10d", i + firstHour);
	printf("\n");

	printf("-------------------------------------------------------------------------------------------------------\n");
	for(int day = 0; day < nDays; ++day){
		int nextRound = -1;

		// This is used to store to pointer of
		// current element in the list for each
		// hour on a given day.
		node *next[nHours];
		for(int i = 0; i < nHours; ++i)
			next[i] = table->table[day][i];

		while(nextRound != 0){
			if(nextRound == -1)
				printf("| %9s |", weekdays[day]);
			else
				printf("|           |");
			nextRound = 0;
			for(int hour = 0; hour < nHours; ++hour){
				if(next[hour] == NULL)
					printf("%9s|", "");
				else {
					teaching *t = (teaching*)next[hour]->data;
					printf(" %7s", t->module->name);
					if(t->type != '\0')
						printf("%c|", t->type);
					else
						printf(" |");
					next[hour] = next[hour]->next;
					if(next[hour] != NULL)
						nextRound++;
				}
			}
			printf("\n");
		}
		printf("-------------------------------------------------------------------------------------------------------\n");
	}
}


/*
 * Allocates memory to a new timetable
 * and sets all the slots to null. 
 *
 * Returns a pointer to the new timetable.
 */
static timetable* create_table()
{
	timetable *timet = malloc(sizeof(timetable));
	for(int day = 0; day < nDays; ++day)
		for(int hour = 0; hour < nHours; ++hour)
			timet->table[day][hour] = NULL;
	return timet;
}


/*
 *	Checks wheather or not a module
 *	clashes with the ones on the given
 *	list.
 *
 *	Returns 1 if it clashes and 0 if it do
 *	not.
 */
static int clashes_with_module(module *m, node *list)
{
	while(list != NULL){
		for(node *p = m->clashes; p != NULL; p = p->next){
			if(compare_module(p->data, ((teaching*)list->data)->module) == 0)
				return 1;
		}
		list = list->next;
	}
	return 0;
}


/*
 *	Compares two teachings. This takes the module
 *	of each teaching and returns the normal
 *	compare_module function.
 *
 *	Returns 0 if they are equal.
 */
static int compare_teachings(const void *t1, const void *t2)
{
	return compare_module(((teaching*)t1)->module, ((teaching*)t2)->module);
}
