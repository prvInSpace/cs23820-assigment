/*
 *	File: module.c
 *
 *	Implementation of a module struct and function related to this.
 *
 *	Author: Preben Vangberg <prv@aber.ac.uk>
 *	Date 12.11.2018
 *	
 * 	Module.c contains functions related to importing, printing, and
 * 	comparing of modules. Also contains a linked list of all modules,
 * 	and functions to remove unnecessary modules from this list.
 *
 *  NOTE: Before freeing the memory used by a module, make sure to clear
 *  the list of clashes, as this list will not be de allocated when free
 *  is called.
 *
 * 	Copyright (C) 2018, Preben Vangberg
 *	Licensed under the MIT license.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "module.h"


static void split_teaching_time(char *teaching, char split, int *v1, int *v2);
static module* create_module(module m);

/*
 * Linkedlist containing all modules.
 */
node *modules;


/*
 *	Reads a module from the given file.
 *
 *	Returns a pointer to the new
 *	module or it returns NULL if
 *	it failed.
 */
void* read_module(FILE *f)
{
	//Used to keep track of the line number
	static int line = 1;

	char name[8];
	int semester;
	char lectures[7];
	char practicals[7];
	int status = fscanf(f, "%7s %d %6s %6s", name, &semester, lectures, practicals);
	if(status != EOF && status == 4){
		module m;
		m.clashes = NULL;
		m.students = 0;
		strcpy(m.name, name);
		m.semester = semester;
		m.lectures = 0;
		m.lectures_length = 0;
		m.practicals = 0;
		m.practicals_length = 0;
		split_teaching_time(lectures, 'L', &m.lectures, &m.lectures_length);
		split_teaching_time(practicals, 'P', &m.practicals, &m.practicals_length);
		line++;
		return create_module(m);
	} 
	else if(status != EOF) {
		printf("\nFailed to read module %s with status %d\n", name, status);
		printf("This is probably because of a syntax error in line %d in modules.txt!\n", line);
		exit(1);
	}
	line++;
	return NULL;
}


/*
 *	Prints a module.
 *
 *	Return 0. This is because
 *	execute_for_each consider
 *	0 a success.
 */
int print_module(void *p)
{	
	module *m = (module*) p;
	printf("%7s %2d %2dL%2d %2dP%2d %3d", m->name, m->semester,
			m->lectures, m->lectures_length,
			m->practicals, m->practicals_length,
			m->students);
	for(node *ms = m->clashes; ms; ms = ms->next){
		printf(" %s", ((module*)ms->data)->name);
	}
	printf("\n");
	return 0;
}


/*
 *	Deletes all the modules with no
 *	students. Also gets rid of any duplicate
 *
 *	Returns number of deleted modules
 */
int delete_modules_with_no_students()
{
	int number_deleted = 0;
	node *p = modules;
	while(p){
		if(p->next && compare_module(p->data, p->next->data) == 0){
			printf("WARNING: Found duplicate of %s in modules.txt! Duplicate was removed\n",
					((module*)p->data)->name);
			clear_list(&((module*)p->next->data)->clashes);
			delete_node(&modules, p->next);
			number_deleted++;
		}
		else if(((module*)p->data)->students == 0){
			node *next = p->next;
			module *m = (module*) p->data;
			clear_list(&m->clashes);
			delete_node(&modules, p);
			number_deleted++;
			p = next;
		}
		else {
			p = p->next;
		}
	}
	return number_deleted;
}


/*
 *	Compares two modules. This is
 *	based on the name of the module.
 *
 *	Returns the difference between the
 *	modules name.
 */
int compare_module(const void *e1, const void *e2)
{
	return strcmp(((module*)e1)->name,((module*)e2)->name);
}


/*
 *	Takes a string with format %d%c%d and splits
 *	it in two. 
 *
 *	Returnes nothing, but places the values at
 *	the end of the two pointers provided
 */
static void split_teaching_time(char *teaching, char split, int  *v1, int *v2)
{
	char buffer[10];
	int i;
	for(i = 0; teaching[i] != split && teaching[i] != '\0'; ++i)
		buffer[i] = teaching[i];
	buffer[i] = '\0';
	*v1 = atoi(buffer);
	i++;
	int j = 0;
	while(teaching[i] != '\0'){
		buffer[j] = teaching[i];
		j++;
		i++;
	}
	*v2 = atoi(buffer);
}


/*
 *	Creates a module based on the module
 *	given.
 *
 *	Returns a pointer to the new module.
 */
static module* create_module(module m)
{
	module *new = malloc(sizeof(module));
	(*new) = m;
	return new;
}




