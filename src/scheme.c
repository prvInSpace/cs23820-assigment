/*
 *	File: scheme.c
 *
 *	A file containing functions related to schemes.
 *
 *	Author: Preben Vangberg <prv@aber.ac.uk>
 *	Date 12.11.2018
 *	
 *	Contains a linked list containing all the schemes
 *	and function related to the schemes.
 *
 *  NOTE: Before freeing the memory used by a scheme, make sure to clear
 *  the list of core modules, as this list will not be de allocated when
 *  free is called.
 *
 * 	Copyright (C) 2018, Preben Vangberg
 *	Licensed under the MIT license.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "list.h"
#include "module.h"
#include "scheme.h"

static scheme* create_scheme(scheme s);

/*
 * Global linked list of schemes.
 */
node *schemes = NULL;


/*
 * Compares two schemes based on their name, and
 * year.
 *
 * Returns the value returned from strcmp, or
 * returns the first minus the second year.
 */
int compare_scheme(const void *e1, const void *e2)
{
	int str = strcmp(((scheme*)e1)->name, ((scheme*)e2)->name);
	if(str == 0){
		return ((scheme*)e1)->year - ((scheme*)e2)->year;
	}
	return str;
}


/*
 * Reads a scheme from the FILE provided.
 * Returns a pointer to the scheme, or
 * NULL if it reached end of file.
 *
 * Will kill the program if the file is
 * not formatted in the correct manner.
 * It will also try to indicate where in
 * the file the problem might be.
 */
void* read_scheme(FILE *f)
{
	//Used to keep track of the line number
	static int line = 1;

	scheme s;
	s.modules = NULL;
	int status = fscanf(f, "%6s %d %d %d",
			s.name,
			&s.year,
			&s.students,
			&s.modules_length);
	if(status != EOF && status == 4){
		for(int i = 0; i < s.modules_length; ++i){
			module m;
			fscanf(f, " %s", m.name);
			module *temp = get_element(modules, &m, &compare_module);
			if(temp != NULL){
				temp->students += s.students;
				add_to_list(&s.modules, temp, &compare_module);
			} else {
				printf("\nERROR: %s is missing a module: %s!\n", s.name, m.name);
				printf("This could be because the module does not exist or is misspelled. It could\n");
				printf("also be because of a syntax error on line %d of schemes.txt\n", line);
				exit(1);
			}
		}
		line++;
		return create_scheme(s);		
	}
	if(status != EOF){
		printf("\nFailed to load scheme %s with status %d\n", s.name, status);
		printf("This is probably because of a syntax error on line %d of schemes.txt\n", line);
		exit(1);
	}
	return NULL;
}


/*
 * Prints the given scheme to stdout
 */
int print_scheme(void *p)
{
	scheme *s = (scheme*) p; 
	printf("%4s %d %2d %d ",
			s->name,
			s->year,
			s->students,
			s->modules_length);
	node *pointer = s->modules;
	while(pointer != NULL){
		printf("%s ",((scheme*)pointer->data)->name);
		pointer = pointer->next;
	}
	printf("\n");
	return 0;
}


/*
 * Deletes all schemes from the global linked list
 * if the scheme contains no students.
 *
 * Returns the number of elements deleted.
 */
int delete_schemes_with_no_students()
{
	int number_deleted = 0;
	node *p = schemes;
	while(p != NULL){
		if(p->next && compare_scheme(p->next->data, p->data) == 0){
			scheme *next = (scheme*) p->next->data;
			printf("WARNING: Found duplicate of %s in schemes.txt! Duplicate was removed\n",
				   next->name);
			clear_list(&next->modules);
			delete_node(&schemes, p->next);
			number_deleted++;
		}
		else if(((scheme*)p->data)->students == 0){
			node *next = p->next;
			scheme *s = (scheme*) p->data;
			clear_list(&s->modules);
			delete_node(&schemes, p);
			number_deleted++;
			p = next;
		}else {
			p = p->next;
		}
	}
	return number_deleted;
}


/*
 * For each module, it goes through all the schemes
 * to find any it is part of. If it is part of a scheme
 * it adds any module to the clashes list that is not
 * currently present.
 */
void find_clashes()
{
	for(node *p = modules; p; p = p->next){
		module *m = (module*) p->data;
		for(node *p1 = schemes; p1; p1 = p1->next){
			scheme *s = (scheme*) p1->data;
			module *e = get_element(s->modules, m, &compare_module);
			if(e){
				for(node *m1 = s->modules; m1; m1 = m1->next){
					if(m->semester  == ((module*) m1->data)->semester && get_element(m->clashes, m1->data, &compare_module) == NULL){
						add_to_list(&m->clashes, m1->data, &compare_module);
					}
				}
			}
		}
	}
}


/*
 * Allocates memory for a scheme, and copies
 * the data from the provided scheme onto the
 * the scheme.
 *
 * Returns a pointer to the new scheme.
 */
static scheme* create_scheme(scheme s)
{
	scheme *new = malloc(sizeof(scheme));
	(*new) = s;
	return new;
}
