/*
 *	File: fileio.h
 *
 *	Contains general functions for file interaction.
 *
 *	Author: Preben Vangberg <prv@aber.ac.uk>
 *	Date 12.11.2018
 *
 *	fileio.c contains functions that is used to prompt the
 *	user for folders, check if files and and folders exist,
 *	and other general file interactions.
 *
 * 	Copyright (C) 2018, Preben Vangberg
 *	Licensed under the MIT license.
 */

#ifndef FILEIO_H
#define FILEIO_H


/*
 *	Prompts the user for a folderpath, and checks whether
 *	or not it as a folder, file, or invalid. Also checks
 *	if the folder contains the neccesary files to complete
 *	the main problem.
 *
 *	Returns a char* that contains the path of the folder.
 */
char* get_folder();


/*
 *	Read a string and remove the tailing \r\n
 *
 * 	This function uses fgets which is a safer way of reading a
 * 	string as it takes a size as its parameter. It have one
 * 	drawback in that it leaves a tailing \r\n on the end. This
 * 	function removes that.
 *
 *  Returns the length of the string
 */
int read_string(char *buffer, int size);


/*
 *	Concatinates the folder and file together with a seperator,
 *	and calls the list.c function that adds all elements in a file
 *	to a certain list.
 *
 *	Returns the number of elements added.
 */
int import_file(char *folder, char *file, void *head, void*(*read_file)(FILE *f), int(*compare)(const void *e1, const void *e2));

#endif
