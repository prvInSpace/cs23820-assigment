/*
 *	File: main.c
 *
 *	The main file of the program.
 *
 *	Author: Preben Vangberg <prv@aber.ac.uk>
 *	Date 12.11.2018
 *	
 *	main.c is the main file, and entry point to
 *	the program. It handles everything from the
 *	reading of arguments, and in a more general
 *	perspective what the program is supposed to
 *	do.
 *
 * 	Copyright (C) 2018, Preben Vangberg
 *	Licensed under the MIT license.
 */

#include <stdio.h>
#include <string.h>

#include "fileio.h"
#include "module.h"
#include "list.h"
#include "room.h"
#include "scheme.h"
#include "table.h"
#include "time.h"

#include "main.h"

#define NAME "OpenPTT"
#define VERSION "1.2.1"

int DEBUG = 0;
int WERROR = 0;
int	INVERTED = 0;

static void evaluate_arguments(int argc, char **argv);
static void initialize();
static void find_module();
static void create_timetable();
static void print_help();

static rooms *r;


int main(int argc, char **argv)
{
	evaluate_arguments(argc, argv);
	initialize();
	find_module();
	create_timetable();
	return 0;
}


/*
 * Prints a warning, and if the --werror flag is set it exit the program
 */
void print_warning(char *msg)
{
	printf("WARNING: %s\n", msg);
	if(WERROR)
		exit(1);
}


/*
 * Evaluates any commandline arguments and sets
 * any flags if it has to.
 */
static void evaluate_arguments(int argc, char **argv)
{
	for(int i = 1; i < argc; ++i){
		if(strcmp("--debug", argv[i]) == 0)
			DEBUG = 1;
		else if(strcmp("--version", argv[i]) == 0){
			printf("%s %s\n", NAME, VERSION);
			printf("Copyright (C) 2018, Preben Vangberg\n");
			printf("%s is licensed under MIT\n", NAME);
			exit(0);
		}
		else if(strcmp("--help", argv[i]) == 0){
			print_help();
		}
		else if(strcmp("--werror", argv[i]) == 0){
			printf("INFO: All warnings treated like errors!\n");
			WERROR = 1;
		} else if(strcmp("--inverted", argv[i]) == 0){
				INVERTED = 1;
		} else {
			printf("Unknown argumment: %s\n", argv[i]);
			printf("Please read README or type add argumment --help\n");
			exit(1);
		}	
	}
}


/*
 * Prompts for a module code then searches for the module. It then prints
 * the module code, the semester, and any clashes the module has
 */
static void find_module()
{
	char buffer[100];
	module search_struct;

	printf("Enter a modulecode: ");
	read_string(buffer, sizeof(buffer));

	strcpy(search_struct.name, buffer);
	module *m = get_element(modules, &search_struct, &compare_module);

	if(m != NULL) {
		printf("%s Semester: %d Clashes with: ", m->name, m->semester);
		for(node* p = m->clashes; p != NULL; p = p->next)
			printf("%s ", ((module*)p->data)->name);
		printf("\n");
	} else
		print_warning("No module with that code!");
}


/*
 * Ask the user which semester they want to build a timetable for. It then
 * tries to create normal timetable. It also creates an extended timetable
 * if the folder contained the rooms.txt file.
 */
static void create_timetable()
{
	char buffer[100];
	int semester;

	printf("\nEnter the semester you want to make a timetable for: ");
	read_string(buffer, sizeof(buffer));
	semester = atoi(buffer);

	timetable *table = build_timetable(semester);
	printf("%44s", "Main Problem\n");
	if(INVERTED)
		print_inverted_table(table);
	else
		print_table(table);

	printf("\nPress enter to continue!");
	read_string(buffer, sizeof(buffer));
	printf("\n");

	if(r != NULL){
		timetable *exttable = build_extended_timetable(semester, r);
		printf("%46s", "Extended Problem\n");
		if(INVERTED)
			print_inverted_table(exttable);
		else
			print_table(exttable);	
		printf("\n");
	}
}


/*
 * Asks for a folder, imports all the files, removes any unnecessary modules
 * and schemes, and create the lists of clashes for each module.
 */
static void initialize()
{
	char *folder = get_folder();	
	printf("\n");

	int nmodule = import_file(folder, "modules.txt", &modules, &read_module, &compare_module);
	int nscheme = import_file(folder, "schemes.txt", &schemes, &read_scheme, &compare_scheme);

	int nmd = delete_modules_with_no_students();
	int nsd = delete_schemes_with_no_students();

	read_time_file(folder);
	r = import_rooms(folder);

	find_clashes();

	printf("\nAdded:   %3d modules and %2d schemes!\n", nmodule, nscheme);
	printf("Removed: %3d modules and %2d schemes!\n", nmd, nsd);
	printf("Total:   %3d modules and %2d schemes!\n\n", nmodule - nmd, nscheme - nsd);

	if(r != NULL)
		printf("Rooms: %d lecture rooms and %d practical rooms!\n\n", r->l_rooms, r->p_rooms);


	if(DEBUG){
		execute_for_each(modules, &print_module);
		printf("\n");
		execute_for_each(schemes, &print_scheme);
		printf("\n");
		print_time_map();
		printf("\n");
	}

}


/*
 * Print the help text.
 */
static void print_help()
{
	char *text[] = {
		"             "NAME " " VERSION "\n",
		"A program that make timetables based on information",
		"given in several files in a folder.\n",
		"--debug",
		"   Makes the program print more debug information\n",
		"--help",
		"   Displays this message and exits\n",
		"--inverted",
		"   Inverts the output of the timetable\n",
		"--version",
		"   Displays the name, version and some general information\n",
		"--werror",
		"   Treat all warnings as errors\n"
	};


	int length = sizeof(text) / sizeof(text[0]);

	for(int i = 0; i < length; ++i)
		printf("%s\n", text[i]);

	exit(0);
}



