/*
 *	File: time.c
 *
 *	Contains the binary-map of lecture times.
 *
 *	Author: Preben Vangberg <prv@aber.ac.uk>
 *	Date 12.11.2018
 *
 *	Contains functions and structs for storing
 *	the data from times.txt. This information
 *	is stored in a binary-map where 0 represents
 *	no lecture, and 1 represents possibilities
 *	for lectures.
 *
 * 	Copyright (C) 2018, Preben Vangberg
 *	Licensed under the MIT license.
 */

#ifndef TIME_H
#define TIME_H

#define nDays 7
#define nHours 9
#define firstHour 9


/*
 * Binary map that represents the
 * available hours.
 */
extern int times[nDays][nHours];


/*
 * Array of char arrays containing
 * the actual name of the weekdays
 * starting from monday
 */
extern char *weekdays[];


/*
 * Reads in the times.txt and
 * stores it in times.
 */
void read_time_file(char *folder);


/*
 * Prints the binarymap.
 */
void print_time_map();

#endif

