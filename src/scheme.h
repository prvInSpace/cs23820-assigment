/*
 *	File: scheme.h
 *
 *	A file containing functions related to schemes.
 *
 *	Author: Preben Vangberg <prv@aber.ac.uk>
 *	Date 12.11.2018
 *
 *	Contains a linked list containing all the schemes
 *	and function related to the schemes.
 *
 *  NOTE: Before freeing the memory used by a scheme, make sure to clear
 *  the list of core modules, as this list will not be de allocated when
 *  free is called.
 *
 * 	Copyright (C) 2018, Preben Vangberg
 *	Licensed under the MIT license.
 */

#ifndef SCHEME_H
#define SCHEME_H

#include <stdlib.h>
#include <stdio.h>

#include "list.h"
#include "module.h"


/*
 * A struct that represents a scheme
 */
typedef struct scheme_struct {
	char name[5];
	int year;
	int students;
	int modules_length;
	node *modules;
} scheme;


/*
 * Global linked list of schemes.
 */
extern node *schemes;


/*
 * Compares two schemes based on their name, and
 * year.
 *
 * Returns the value returned from strcmp, or
 * returns the first minus the second year.
 */
int compare_scheme(const void *e1, const void *e2);


/*
 * Reads a scheme from the FILE provided.
 * Returns a pointer to the scheme, or
 * NULL if it reached end of file.
 *
 * Will kill the program if the file is
 * not formatted in the correct manner.
 * It will also try to indicate where in
 * the file the problem might be.
 */
void* read_scheme(FILE *f);


/*
 * Prints the given scheme to stdout
 */
int print_scheme(void *p);

/*
 * Deletes all schemes from the global linked list
 * if the scheme contains no students.
 *
 * Returns the number of elements deleted.
 */
int delete_schemes_with_no_students();


/*
 * Allocates memory for a scheme, and copies
 * the data from the provided scheme onto the
 * the scheme.
 *
 * Returns a pointer to the new scheme.
 */
void find_clashes();

#endif
